Mulai bulan Juni 2021, kita (UX Engineer) akan coba untuk memulai penyusunan sebuah dokumentasi yang berisi aturan-aturan dasar dalam coding UI/UX. Mulai dari manajemen directory sampai dengan aturan penamaan sebuah class. Untuk tim UX engineer silakan untuk urun rembuk beberapa hal yang mungkin bisa meningkatkan efisiensi proses kerja kita. Feel free untuk edit dokumentesi ini juga ya.

# :zap: Chapter 1: Pendahuluan

Berikut beberapa kisah di belakang ini semua, mengapa kita sudah harus menyelaraskan standar aturan dasar dalam coding UI/UX.

## Latar Belakang 
* Beberapa fitur yang di Kompascom mulai diaplikasikan di beberapa situs di lingkungan KG Media, karena itu setiap ada request fitur tertentu, kita harus berpikir bahwa fitur tersebut bisa dengan mudah diaplikasikan di situs KG Media lainnya. Alias istilah kerennya ***Modular Component***. Nanti detail aturannya akan dibahas di bawah.
* Mulai tahun 2021, [Google akan menggunakan **Web Vitals** sebagai acuan mereka dalam merangking website yang tampil di search engine mereka](https://www.searchenginejournal.com/google-core-web-vitals-ranking-signals/387142/) yang artinya dalam coding, kita harus menggunakan poin-poin hasil scoring mereka.

## Batasan Masalah
Nantinya ini berlaku untuk project situs on forward. Tidak berlaku untuk situs yang sudah terlanjur berjalan sebelum aturan ini disahkan. Karena akan berpotensi merepotkan tim developer bila kita ubah drastis. Hanya saja jika tidak berhubungan langsung dengan tim developer (misal seperti manajemen folder dummy), akan lebih baik jika kita ubah sesuai dengan dokumentasi ini.

## Referensi teori 
Nantinya semua aturan ini mengacu kepada poin-poin metrik dari beberapa tools berikut ini.

**Web Vitals** [https://web.dev/vitals/](https://web.dev/vitals/)
* Largest Contentful Paint (LCP)
* First Input Delay (FID)
* Cumulative Layout Shift (CLS)

**Lighthouse** 
* Accessibility
* Performance

## Goals
* Tujuan dibuatnya aturan-aturan dasar coding ini tak lain agar cara kerja kita semakin efisien. 
* Selain itu semoga setelah ini kita akan punya **UX Engineering Code System**.
* Serta tujuan jangka panjang semoga kita bisa punya **Design System** keseluruhan se-KG Media bersama dengan tim UI Designer juga.

# :zap: Chapter 2: Pembahasan

Berikut ini detail pembahasan dengan dibagi menjadi beberapa topik. Skuy kita bahas detailnya!

## Tools

Basic tools yang digunakan adalah: 

* [**Gulp**](https://gulpjs.com/), sebagai basic tools untuk otomatisasi proses compiling, minifying, dll. Tentunya harus sudah ter-install Nodejs dan segala macamnya. 

Untuk plugin gulp yang kita gunakan adalah:

* [**Gulp-Handlebars**](https://www.npmjs.com/package/gulp-compile-handlebars), sebagai templating HTML. Basic-nya dari Handlebars, tetapi yang kita gunakan adalah terkhusus Gulp-Handlebars. 
* Untuk helper-nya kita menggunakan [**Handlebars-helpers**](https://www.npmjs.com/package/handlebars-helpers). Minim dokumentasi sih, karena memang bukan standar yang dipakai UX engineer di luar sana. Hanya saja sejauh ini tools ini yang cocok dengan output yang kita butuhkan. 
* [**Gulp-Sass**](https://www.npmjs.com/package/gulp-sass), sebagai compiler ke CSS. 
* [**Gulp-Autoprefixer**](https://www.npmjs.com/package/gulp-autoprefixer), sebagai otomatisasi prefix css browser yang kita support.

Untuk optional, ada beberapa plugin gulp lainnya seperti:

* [**Gulp-Include**](https://www.npmjs.com/package/gulp-include), sebagai compiling file modular javascript menjadi satu file induk. Cara kerja mirip manajemen direcoty-nya Sass.

**Notes**: *Untuk version gulp, nodejs, dst yang kita gunakan nanti dibahas tersendiri ya, apakah sebaik-nya disamakan juga*. 

## JSON

* Kebutuhan JSON adalah untuk looping data aliran list berita, dan konten artikel.
* JSON bersifat offline, kita buat sendiri dan nantinya akan di-compile lewat bantuan handlebars.
* Kenapa kita butuh JSON dalam konteks ini adalah agar tampilan dummy kita mendekati kondisi real konten yang akan ditayangkan, tidak berisi hanya lorem ipsum.

**Notes**: *Idealnya kita harus punya JSON masing-masing site (Kompascom, Kompasiana, Grid Network), nanti dibahas tersendiri*. 

## HTML Naming Convention

Ini adalah pembahasan topik penamaan class HTML. Pada dasarnya kita menggunakan acuan penamaan **[BEM]**(http://getbem.com/naming/), akan tetapi akan ada modifikasi sesuai dengan kebutuhan kita sendiri. 

#### :abc: Tag HTML

Tag HTML sebisa mungkin semantic, tujuannya adalah agar coding kita ramah SEO dan good accessibility, **[berikut referensi bacaannya](https://medium.com/swlh/semantic-html-for-better-accessibility-a54d0742033f)**. Tools-nya review-nya di Lighthouse ada poin accessibility. Banyak poin-poinnya sih, tapi berikut ada contoh basic-nya, 

````html
<!-- salah -->
<div class="header">
    <div class="nav">
        <div class="nav-item">Home</div>
        <div class="nav-item">About</div>
        <div class="nav-item">Contact Us</div>
    </div>
</div>

<!-- benar -->
<header class="header">
    <nav class="nav">
        <div class="nav-item">Home</div>
        <div class="nav-item">About</div>
        <div class="nav-item">Contact Us</div>
    </nav>
</header>
````

Contoh lain dari penggunaan tag semantic yang benar adalah tag digunakan sesuai fungsi tepat gunanya. Case study: Jika ingin menggunakan tag navigasi ke halaman lain, sebaiknya gunakan tag `a href` daripada `button`. 

````html
<!-- kurang tepat -->
<button onclick="location.href='//domain/about/'" type="button">About</button>

<!-- tepat guna -->
<a href="//domain/about/">About</a>
````

Untuk keperluan SEO, dalam setiap halaman HTML sebaiknya harus mengandung HANYA SEBUAH `H1`, kemudian sisanya `H2`, `H3`, dst. Karena kita adalah situs berita, biasanya layout dibedakan 2, indeks dan read artikel. Berikut pengaturan `H1` `H2` yang bisa kita pakai.

````html
<!-- contoh untuk indeks -->
<section class="headline">
    <div class="headline-item"><h1><a href="#">Judul Satu</a></h1></div>
    <div class="headline-item"><h2><a href="#">Judul Dua</a></h2></div>
    <div class="headline-item"><h2><a href="#">Judul Tiga</a></h2></div>
</section>

<!-- contoh untuk artikel -->
<h1>Judul Artikel</h1>
<article>
    <h2>Sub Judul</h2>
    <p>Isi artikel</p>
</article>
````

Sebisa mungkin setiap tag html yang akan di-styling, harus diberi nama class. Artinya kita harus styling di class-nya, bukan tag html-nya. Tapi nggak semua tag html harus diberi class ya. Menurut kebutuhan saja, yang penting jika ingin styling di tag tersebut, wajib styling di class-nya. Contohnya:

````html
<!-- contoh yang tidak direkomendasikan -->
<nav>
    <ul>
        <li>Menu 1</li>
        <li>Menu 2</li>
    </ul>
</nav>
````

````scss
// contoh yang tidak direkomendasikan
nav ul li {
    display: inline-block;
}
````

````html
<!-- contoh yang baik -->
<nav>
    <ul class="nav">
        <li class="nav-item">Menu 1</li>
        <li class="nav-item">Menu 2</li>
    </ul>
</nav>
````

````scss
// contoh yang baik
.nav {
  display: block;
  &-item {
    display: inline-block;
  }
}
````

**NOTE**: Hal tersebut di atas ada pengecualian untuk tag di body artikel. Karena kita ada CMS, penulisan body artikel tag html-nya memakai tag dasar, misalnya `H2` `p` `ul` `li`. Maka kita harus menggunakan bantuan class induk untuk styling tag tersebut. Contoh:

````html
<article class="read-content">
    <p><strong>KOMPAS.com</strong> - Jepang dikabarkan telah melewati keadaan darurat virus corona atau Covid-19.</p>
    <h2>Tetap menjaga jarak sosial</h2>
    <p>Dilansir <a href="#"><em>Reuters</em></a> Selasa tanggal 11 Juni 2020</p>
</article>
````

````scss
.read-content {
  h2 {
    font-size: 1rem;
  }
  p {
    margin: 1rem 0;
  }
}
````

#### :abc: Modular CSS Component

Dalam pembuatan suatu fitur, dari awal kita harus selalu berkonsep apakah fitur tersebut kemungkinan akan dipakai di situs keluarga KG Media yang lainnya atau hanya spesifik di salah satu situs saja. Jika ada kemungkinan nantinya bisa scale up ke situs KG Media, maka pembuatan fitur tersebut dari mulai nama class dan styling harus general. Tapi lebih amannya sih dalam pembuatan fitur apapun sebisa mungkin harus fleksibel.

Dalam pembuatan suatu fitur atau elemen, kita akan memakai konsep modular CSS. Artinya komponen tersebut berdiri sendiri bisa dipindah ke berbagai layout, jadi dari mulai margin/padding sebisa mungkin fleksibel.

Contoh modular CSS.

Jika kita membuat sebuah navigasi di header, daripada membuat sebuah class `#header ul` lebih baik menggunakan nama sesuai fungsi komponen tersebut, misalnya `.nav`.

## Handlebars
## Sass
## Javascript
## Asset (Icon)
## AMP Page


